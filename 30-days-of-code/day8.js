function processData(input){
	input=input.split('\n');
	var n=parseInt(input.shift());
	var phones=input.slice(0, input.length - n);
	var queries=input.slice(input.length - n, input.length);
	var pb={};
	for(var i=0; i<(2 * n); i++) {
		if( phones[ i ][ 0 ]==='0' || /\d/.test(phones[ i ]) )
			continue;
		else if( i % 2===0 ) {
			if( phones[ i + 1 ].length>8 )
				continue;
			else
				pb[ phones[ i ] ]=phones[ i + 1 ];
		}
	}
	queries.forEach(function(item){
		                var result='Not found';
		                if( pb.hasOwnProperty(item)===true )
			                result=item + '=' + pb[ item ];
		                console.log(result);
	                }
	);
}
