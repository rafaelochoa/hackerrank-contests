function processData(input){
	var result;
	input=input.trim()
	           .split(' ');
	result=gcd(input[ 0 ], input[ 1 ]);
	function gcd(a, b){
		if( b==0 ) {
			return a;
		}
		else {
			return gcd(b, a % b);
		}
	}

	console.log(result);
}
