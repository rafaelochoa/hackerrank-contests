function processData(input){
	var n;
	input=input.split('\n')
	           .map(Number);
	n=input[ 0 ];
	if( n<1 || n>1000 ) {
		return;
	}
	for(var i=0; i<input.length - 1; i++) {
		var num=input[ i + 1 ];
		if( num<1 || num>2147483648 ) {
			return;
		}
		var binary='';
		while(num!==0) {
			binary+=(num % 2).toString();
			num=Math.floor(num / 2);
		}
		console.log(binary.split('')
		                  .reverse()
		                  .join('')
		);
	}
}
