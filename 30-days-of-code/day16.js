function processData(input){
	var minDiff;
	var result=[];
	var numbers=input.split('\n')[ 1 ].split(' ')
	                                  .map(Number);
	numbers=merge(numbers);
	minDiff=numbers[ 1 ] - numbers[ 0 ];
	numbers.forEach(function(num, i){
		                var diff=num - numbers[ i - 1 ];
		                if( diff<minDiff ) {
			                result=[];
			                result.push(numbers[ i - 1 ]);
			                result.push(num);
			                minDiff=diff;
		                }
		                else if( diff==minDiff ) {
			                result.push(numbers[ i - 1 ]);
			                result.push(num);
		                }
	                }
	);
	console.log(result.join(' '));
	function merge(arr){
		if( arr.length<2 )
			return arr;
		var mid=Math.floor(arr.length / 2);
		var left=arr.slice(0, mid);
		var right=arr.slice(mid, arr.length);
		return mergeSort(merge(left), merge(right));
	}

	function mergeSort(left, right){
		var result=[];
		while(left.length && right.length) {
			if( left[ 0 ]<=right[ 0 ] )
				result.push(left.shift());
			else
				result.push(right.shift());
		}
		while(left.length)
			result.push(left.shift());
		while(right.length)
			result.push(right.shift());
		return result;
	}
}
