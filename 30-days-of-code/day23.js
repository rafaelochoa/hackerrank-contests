function processData(root){
	var queue=[];
	queue.push(root)
	while(Object.keys(queue).length) {
		var node=queue.shift();
		console.log(node.data + ' ');
		if( node.left )
			queue.push(node.left);
		if( node.right )
			queue.push(node.right);
	}
}
