function processData(input){
	input=input.split('\n')
	           .map(Number);
	var t=input.shift();
	var limit=2 * Math.pow(10, 9);
	if( t>=1 && t<=20 )
		input.forEach(function(item){
			              if( item>=1 && item<=limit )
				              console.log((isPrime(item)===true) ? 'Prime' :'Not prime');
		              }
		);
	function isPrime(num){
		var result=true;
		if( num===0 || num===1 )
			result=false;
		else if( num>3 )
			for(var i=Math.floor(Math.sqrt(num)); i>=2; i--)
				if( num % i==0 || num % 2==0 || num % 3==0 )
					result=false;
		return result;
	}
}
