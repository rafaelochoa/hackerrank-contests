function Palindrome(){
	this.stack=[];
	this.queue=[];
	this.pushCharacter=pushCharacter;
	this.enqueueCharacter=enqueueCharacter;
	this.popCharacter=popCharacter;
	this.dequeueCharacter=dequeueCharacter;
	function pushCharacter(ch){
		this.stack.push(ch);
	}

	function enqueueCharacter(ch){
		this.queue.push(ch);
	}

	function popCharacter(){
		return this.stack.pop();
	}

	function dequeueCharacter(){
		return this.queue.shift();
	}
}
