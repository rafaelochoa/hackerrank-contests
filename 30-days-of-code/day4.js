function Person(initial_Age) {
    // Add some more code to run some checks on initial_Age
    if (initial_Age < 0) {
        initial_Age = 0;
        console.log('This person is not valid, setting age to 0.');
    }
    this.age = initial_Age;
    this.amIOld = function () {
        // Do some computations in here and print out the correct statement to the console
        if (this.age < 13)
            console.log('You are young.');
        else if (this.age >= 13 && this.age < 18)
            console.log('You are a teenager.');
        else if (this.age >= 18)
            console.log('You are old.');
    };
    this.yearPasses = function () {
        // Increment the age of the person in here
        this.age++;
    };
}
