function processData(input) {
    var numbers = input.split('\n').map(Number);
    var result = -1;
    if (numbers.length === 3) {
        result = Math.round(((numbers[1] / 100) * numbers[0]) + ((numbers[2] / 100) * numbers[0]) + numbers[0]);
    }

    console.log('The final price of the meal is $' + result + '.');
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
    processData(_input);
});
