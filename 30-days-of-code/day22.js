function processData(root){
	return (!root) ? 0 :(Math.max(this.getHeight(root.left), this.getHeight(root.right)) + 1);
}
